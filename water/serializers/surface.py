from water.cache import CacheMiss, parameter_cache
from water.models.surface import Fraction

NORM_KEYS = [
    "aa_bcr",
    "aa_eu",
    "sum",
    "mac",
    "min_val",
    "max_val",
    "aa_min_val",
    # 'biote',
    "summer",
    "winter",
    "p10",
    "p90",
]


def serialize_norm(norm):
    values = dict()
    for key in NORM_KEYS:
        val = getattr(norm, key)
        if val is not None:
            values[key] = float(val)

    return {
        "options": {
            "waterbody": norm.id_waterbody_id,
        },
        "values": values,
    }


def make_fraction(instance):
    return instance.frac_en, dict(
        fr=instance.frac_fr,
        nl=instance.frac_nl,
    )


FRACTION = {k: v for k, v in map(make_fraction, Fraction.objects.all())}


def fname(param, frac):
    if frac is not None:
        frac_fr = FRACTION[frac]["fr"]
        frac_nl = FRACTION[frac]["nl"]
        return {
            # str is a temporary quick fix for the case name does not exist in a given langage
            "fr": "{} ({})".format(str(param.par_fr), frac_fr),
            "nl": "{} ({})".format(str(param.par_nl), frac_nl),
            "en": "{} ({})".format(str(param.par_en), frac),
        }

    return {"fr": str(param.par_fr), "nl": str(param.par_nl), "en": str(param.par_en)}


def serialize_parameter(param):
    """Serialize a models.surface.Parameter"""
    try:
        return parameter_cache.find(param.gid)
    except CacheMiss:
        pr = param.parresult_set.first()  # fragile but it works
        groups = []
        norms = []
        if pr is not None:
            for gp in pr.grouppar_set.filter(id_group__group_sdi=True).select_related(
                "id_group"
            ):
                group = gp.id_group  # so weird naming, this is not an id
                groups.append(
                    {
                        "id": group.gid,
                        "sort": group.order_sdi,
                        "name": {
                            "fr": group.group_fr,
                            "nl": group.group_nl,
                        },
                    }
                )

            norms = [
                serialize_norm(n) for n in pr.norms_set.filter(end_date__isnull=True)
            ]

        frac = pr.frac if pr is not None else None

        ser_param = {
            "id": param.gid,
            "code": param.short_name,
            "name": fname(param, frac),
            "unit": param.unit_param,
            "groups": groups,
            "norms": norms,
        }
        parameter_cache.push(param.gid, ser_param)

        return ser_param


def parse_float(s):
    result = float(s.replace(",", "."))
    # print('parse_float({}) => {}'.format(s, result))
    return result


def serialize_timeserie_record(rec):
    """Serialize a models.surface.Results"""
    txt = rec.validated_txt_value.strip()

    if txt.startswith("<"):
        return [rec.id_sample.sampling_date.isoformat(), parse_float(txt[1:]), 0.5]

    return [rec.id_sample.sampling_date.isoformat(), parse_float(txt), 1]
