from datetime import timedelta, datetime
from collections import namedtuple
from django.conf import settings


class CacheMiss(Exception):
    pass


CacheRecord = namedtuple("CacheRecord", ["item", "eol"])


class Cache:
    def __init__(self, seconds):
        self._data = dict()
        self._lifetime = timedelta(seconds=seconds)

    def push(self, key, item):
        self._data[key] = CacheRecord(item, datetime.now() + self._lifetime)

    def find(self, key):
        if key not in self._data:
            raise CacheMiss()
        record = self._data[key]
        now = datetime.now()
        if record.eol < now:
            self._data.pop(key)
            raise CacheMiss()

        return record.item


PARAMETER_CACHE_TTL = getattr(settings, "WATER_PARAMETER_CACHE_TTL", 3600 * 24 * 7)
STATIONCOUNT_CACHE_TTL = getattr(
    settings, "WATER_STATIONCOUNT_CACHE_TTL", 3600 * 24 * 7
)
DATACOUNT_CACHE_TTL = getattr(settings, "WATER_DATACOUNT_CACHE_TTL", 3600 * 24)

parameter_cache = Cache(PARAMETER_CACHE_TTL)
stationcount_cache = Cache(STATIONCOUNT_CACHE_TTL)
datacount_cache = Cache(DATACOUNT_CACHE_TTL)
