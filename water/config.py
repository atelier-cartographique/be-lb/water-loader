from collections import namedtuple
from urllib.parse import unquote
import base64
import json
from datetime import datetime

Config = namedtuple("Config", ["kind", "parameters", "stations", "start", "end"])


def parse_config(s):
    unquoted = unquote(s)
    decoded = base64.b64decode(unquoted).decode("utf-8")
    data = json.loads(decoded)
    window = data["w"]
    start = None
    end = None
    if window is not None:
        start_ts = window[0] // 1000
        end_ts = window[2] // 1000
        if start_ts > 0:
            start = datetime.fromtimestamp(start_ts).date()
        if end_ts > 0:
            end = datetime.fromtimestamp(end_ts).date()

    return Config(
        data["k"],
        data["p"],
        data["s"],
        start,
        end,
    )
