from django.apps import AppConfig


class WaterConfig(AppConfig):
    name = 'water'
    label = 'water'
    verbose_name = 'Water'
    geodata = True
