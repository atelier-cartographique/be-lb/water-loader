"""water URL Configuration
"""

from django.urls import path

from water import views


def prefixed(p):
    return "water/{}".format(p)


urlpatterns = [
    # catalogs
    path(
        prefixed("surface/parameters/"),
        views.get_catalog_surface,
        name="water.get_catalog_surface",
    ),
    path(
        prefixed("ground/parameters/quality/"),
        views.get_catalog_ground_quality,
        name="water.get_catalog_ground_quality",
    ),
    path(
        prefixed("ground/parameters/quantity/"),
        views.get_catalog_ground_quantity,
        name="water.get_catalog_ground_quantity",
    ),
    # timeseries
    path(
        prefixed("surface/timeserie/<int:station>/<int:parameter>"),
        views.get_timeserie_surface,
        name="water.get_timeserie_surface",
    ),
    path(
        prefixed("ground/timeserie/quality/<int:station>/<int:parameter>"),
        views.get_timeserie_ground_quality,
        name="water.get_timeserie_ground_quality",
    ),
    path(
        prefixed("ground/timeserie/quantity/<int:station>/<int:parameter>"),
        views.get_timeserie_ground_quantity,
        name="water.get_timeserie_ground_quantity",
    ),
    path(
        prefixed("ground/timeserie/parameter/count/<int:station>"),
        views.get_station_parameter_ground_count,
        name="water.get_station_parameter_ground_count",
    ),
    path(
        prefixed("surface/timeserie/parameter/count/<int:station>"),
        views.get_station_parameter_surface_count,
        name="water.get_station_parameter_surface_count",
    ),
    path(
        prefixed("surface/timeserie/station/count/<int:parameter>"),
        views.get_station_surface_count,
        name="water.get_station_surface_count",
    ),
    path(
        prefixed("ground/timeserie/station/count/<int:parameter>"),
        views.get_station_ground_count,
        name="water.get_station_ground_count",
    ),
    # layers
    path(
        prefixed("surface/layer"),
        views.get_layer_surface,
        name="water.get_layer_surface",
    ),
    path(
        prefixed("ground/layer/quality"),
        views.get_layer_ground_quality,
        name="water.get_layer_ground_quality",
    ),
    path(
        prefixed("ground/layer/quantity"),
        views.get_layer_ground_quantity,
        name="water.get_layer_ground_quantity",
    ),
    path(
        prefixed("hydro/layer-info"), views.get_hydro_info, name="water.get_hydro_info"
    ),
    path(
        prefixed("hydro/layer-data/<metadata_id>"),
        views.get_hydro_layer,
        name="water.get_hydro_layer",
    ),
    # CSV
    path(
        prefixed("csv/<lang>/<config>"),
        views.get_csv,
        name="water.get_csv",
    ),
    # norms
    path(
        prefixed("info/norms"),
        views.get_norm_names,
        name="water.get_norm_names",
    ),
    # water bodies
    path(
        prefixed("info/waterbodies"),
        views.get_water_bodies,
        name="water.get_water_bodies",
    ),
]
