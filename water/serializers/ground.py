from ..models.ground import make_prefix
from datetime import timezone, datetime

# NORM_KEYS = [
#     'norme_eaupotable',
#     'normedce_nqe_br01',
#     'normedce_nqe_br02',
#     'normedce_nqe_br03',
#     'normedce_nqe_br04',
#     'normedce_nqe_br05',
# ]


def update_norms(ns, name, val, **kwargs):
    if val is not None:
        values = {name: float(val)}  # norme eau potable is a string :/
        options = kwargs
        ns.append(
            {
                "options": options,
                "values": values,
            }
        )


def serialize_norm(norm):
    norms = []
    update_norms(norms, "dce_nqe", getattr(norm, "normedce_nqe_br01"), waterbody=1)
    update_norms(norms, "dce_nqe", getattr(norm, "normedce_nqe_br02"), waterbody=2)
    update_norms(norms, "dce_nqe", getattr(norm, "normedce_nqe_br03"), waterbody=3)
    update_norms(norms, "dce_nqe", getattr(norm, "normedce_nqe_br04"), waterbody=4)
    update_norms(norms, "dce_nqe", getattr(norm, "normedce_nqe_br05"), waterbody=5)

    update_norms(norms, "eaupotable", getattr(norm, "norme_eaupotable"))

    return norms


class CacheMiss(Exception):
    pass


class ParameterCache:
    _data = dict()

    @classmethod
    def push(cls, id, ser_param):
        cls._data[id] = ser_param

    @classmethod
    def find(cls, id):
        if id in cls._data:
            return cls._data[id]
        raise CacheMiss()


def force_string(v):
    return "{}".format(v)


def serialize_parameter_quality(param):
    """Serialize a models.ground.GwQualParametre"""
    try:
        return ParameterCache.find(param.pk)
    except CacheMiss:

        groups = []
        for gp in param.gwqualingrouppar_set.filter(
            noidgroupeparametre__group_sdi=True
        ):
            group = gp.noidgroupeparametre  # so weird naming
            groups.append(
                {
                    "id": group.pk,
                    "sort": group.order_sdi,
                    "name": {
                        "fr": force_string(group.nomgroupe),
                        "nl": force_string(group.naamgroep),
                    },
                }
            )

        norms = serialize_norm(param)

        ser_param = {
            "id": param.pk,
            "code": param.nomparametre,
            "name": {
                "fr": force_string(param.nomparametre),
                "nl": force_string(param.naamparametre),
                "en": force_string(param.name_parametre),
            },
            "unit": param.uniteparametre,
            "groups": groups,
            "norms": norms,
        }
        ParameterCache.push(param.pk, ser_param)

        return ser_param


def parse_float(s):
    result = float(s.replace(",", "."))
    # print('parse_float({}) => {}'.format(s, result))
    return result


def serialize_timeserie_record_quality(rec):
    """Serialize a models.ground.GwQualResultat"""
    txt = rec.valeurtexte
    num = rec.valeurnum

    if num is not None:
        return [rec.idechantillon.date_ech.isoformat(), num, 1]

    if txt is not None and txt.startswith("<"):
        return [
            rec.idechantillon.date_ech.isoformat(),
            parse_float(txt[1:].strip()),
            0.5,
        ]

    return [rec.idechantillon.date_ech.isoformat(), 0, 0]


def serialize_timeserie_record_quantity(code, absolute):
    """Serialize a models.ground.piezo_model"""
    prefix = make_prefix("data_{}".format(code))
    time_field = prefix("time")
    value_field = prefix("hp") if absolute else prefix("pfsol")

    def inner(rec):
        dt = getattr(rec, time_field)
        ts = dt.timestamp()
        return [
            datetime.fromtimestamp(ts, timezone.utc).isoformat(),
            float(getattr(rec, value_field)),
            1,
        ]

    return inner
