# BruWater

Portail de visualisation et distribution des données relatives aux eaux souterraines et de surface de la région de Bruxelles-Capitale.


## Installation

Ceci est une application Django/geodata. 

En premier lieu, installer avec `setuptools` ou `pip` dans l'environnement de destination, par exemple comme ceci:

```bash
pip install -U git+https://gitlab.com/atelier-cartographique/be-lb/water-loader.git
```


En second, configurer Django pour charger cette application.

```python
INSTALLED_APPS.append('water')
```



Et finalement (et optionnellement) configurer les entrées suivantes:

 - `WATER_HYDRO_MAP` l'identifiant d'une carte présente sur geodata (couches et style hydrographie).
 - `WATER_EXCLUDE_PIEZO_STAT` une liste d'identifiants numériques (`mon_be_code`) pour les stations pour lesquelles il ne faut pas produire de statistiques.
 - `WATER_PIEZO_TREND_THRESHOLD` delta évaluation de la tendance, en mètres, valeu par défaut: 0.05.
 - `CLIENTS` ajouter le client `timeserie`


exemple: 

```python
WATER_HYDRO_MAP = '9c0a6a1e-36b5-4729-bbb5-1cf404d3a265'
WATER_EXCLUDE_PIEZO_STAT = [123, 456]

CLIENTS = {
    # ... autrs clients
    {
        'route': 'timeserie',
        'name': {
            'fr': 'BruWater',
            'nl': 'BruWater',
        },
    },
}
```




Par ailleurs, il faut nécessairement avoir intégrer `timeserie` à la compilation des clients.
